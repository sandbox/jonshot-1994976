<div class="propertyListing-image">
  <?php if($fields['field_featured']->content): ?>
  <p class="propertyListing-image-featured">featured property</p>
  <?php endif; ?>
  <?php print l($fields['field_images']->content, 
          'node/' . (int)$fields['view_node']->raw,
          array('html' => TRUE,
              'attributes' => array('class' => array('propertyListing-image-link'))
              )); ?>
</div>
<?php unset($fields['field_images']); ?>
<div class="propertyListing-details">
  <hgroup>

    <h3 class="propertyListing-details-price">
      &pound;<?php print $fields['field_price']->content; ?>
      <?php if($fields['field_price_qualifier']->content): ?>
      <span class="propertyListing-details-price-qualifier">
        (<?php print $fields['field_price_qualifier']->content ?>)
      </span>      
      <?php endif; ?>
    </h3>
    <h2 class="propertyListing-details-address">      
      <?php print $fields['title']->content; ?>
    </h2>
  </hgroup>
  <?php print $fields['body']->content;?>
  <p class="propertyListing-details-readMore"><?php print $fields['view_node']->content; ?></p>
  <p class="propertyListing-details-agentReference">
    contact: 0191 519 7372 (ref: <?php print $fields['field_agent_reference']->content; ?>)
  </p>
</div>
