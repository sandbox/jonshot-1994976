<?php

$content_type = array(
      'type' => 'vebraimport_property',
      'name' => t('Property'),
      'base' => 'node_content',
      'description' => t('A property imported from Vebra'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
  );

$content_type = node_type_set_defaults($content_type);
node_type_save($content_type);
node_add_body_field($content_type, 'description');


