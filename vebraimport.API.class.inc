<?php    
namespace YDD\Vebra;

use \YDD\Vebra\TokenStorage\Drupal as TokenStorageDrupal,
    \Buzz\Client\Curl as BuzzClientCurl,
    \Buzz\Message\Factory\Factory as BuzzMessageFactory;

class vebraimport {  
  public static function init($username, $password, $dataFeedID) {
    return new API(
      $dataFeedID,
      $username,
      $password,
      new TokenStorageDrupal(),
      new BuzzClientCurl(),
      new BuzzMessageFactory()
    );
  } 
}
