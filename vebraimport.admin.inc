<?php

/**
 *
 * Vebra API settings form
 */

function vebraimport_settings() {
  
  $form = array();
  
  $form['token'] = array(
      '#markup' => '<p><strong>Active token:</strong> ' . variable_get('vebraimport_token','') . '</p>'
  );
  
  $form['username'] = array(
      '#type' => 'textfield',
      '#title' => t('Username'),
      '#description' => t('Your Vebra API username'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => variable_get('vebraimport_username', ''),
  );
  
   $form['password'] = array(
      '#type' => 'textfield',
      '#title' => t('Password'),
      '#description' => t('Your Vebra API password'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => variable_get('vebraimport_password', ''),
  );
   
  $form['datafeedid'] = array(
      '#type' => 'textfield',
      '#title' => t('Data feed ID'),
      '#description' => t('Your Vebra API data feed ID'),
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => variable_get('vebraimport_datafeedid', ''),
  );
  
  $form['#submit'][] = 'vebraimport_settings_submit';
  
  return system_settings_form($form);
}

/**
 *
 * Implements hook_submit 
 */

function vebraimport_settings_submit($form, &$form_state) {

  variable_set('vebraimport_username', $form_state['values']['username']);
  variable_set('vebraimport_password', $form_state['values']['password']);
  variable_set('vebraimport_datafeedid', $form_state['values']['datafeedid']);
  
  vebraimport_initial_import();
}

/**
 * Runs initial import
 */

function vebraimport_initial_import() {
    
    $success = FALSE;
    
    if(variable_get('vebraimport_initial_import', FALSE) == FALSE) {
      return;
    }

    $vebra_api = vebraimport_init();
    
    if($vebra_api) {
      
     try {
        $branch_summaries = $vebra_api->getBranches();      
        $branches = array();

        foreach ($branch_summaries as $branch_summary) {
          $client_id = $branch_summary->getClientId();
          $branches[$client_id] = $vebra_api->getBranch($client_id);
        }

        $property_summaries = array();      

        foreach($branches as $client_id => $branch) {
          $property_summaries[$client_id] = $vebra_api->getPropertyList($client_id);
        }

        unset($branches);

        $properties = array();

        foreach ($property_summaries as $client_id => $property_list) {
          
          foreach($property_list as $property) {  
            $prop_id = $property->getPropId();
            $properties[$prop_id] = $vebra_api->getProperty($client_id, $prop_id);
          }
          
        }
        
        unset($property_summaries);         
        
        vebraimport_import_properties($properties, TRUE);
        variable_set('vebraimport_initial_import', FALSE);
        variable_set('vebraimport_timestamp',new DateTime());

        $success = TRUE;
      } catch(Exception $e) {}     
    }
    
    if(TRUE === $success) {
      watchdog('vebraimport', 'Initial import successful', NULL, WATCHDOG_INFO);              
    } else {
      watchdog('vebraimport', 'Initial import failed.', NULL, WATCHDOG_ERROR); 
    }
    
    return $success;      
}