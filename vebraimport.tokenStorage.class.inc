<?php
namespace YDD\Vebra\TokenStorage;

/**
 * A filesystem based Token Storage system
 */
class Drupal extends Base
{

    /**
     * The path where the token files are stored
     * @var string
     */
    protected $tokenName = 'vebraimport_token';

    /**
     * Constructor
     * @param string $tokenName The name of the token in the variables table
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Load the token from variables table
     */
    protected function load()
    {                
        if (FALSE !== ($token = variable_get($this->tokenName, FALSE))) {
            $this->token = $token;
        } else {
          watchdog('vebraimport', 'Token could not be loaded', NULL, WATCHDOG_ALERT);
        }
    }

    /**
     * Save the token to variables table
     */
    protected function save()
    { 
      //TODO: Test this
      //Think there is an issue if trying to authenticate when token already exists,
      //NULL is returned so set to an empty string instead...
      if($this->token === NULL) {
        $this->token = '';
      }

      variable_set($this->tokenName, $this->token);
    }
}